from db.DML import DML
from sqlalchemy.sql import func
from db.conector import Base
from sqlalchemy import Column, String, Integer, Float, DateTime


class Cliente(Base):
    __tablename__ = 'clientes'
    id = Column(Integer, primary_key= True)
    nombre = Column(String(20))
    apellido = Column(String(40))
    direccion = Column(String(100))
    telefono = Column(String(12))
    monto_apertura = Column(Float())
    created_at = Column(DateTime(timezone=True), server_default=func.now())



    def menu(self):
        opc = 0
        salir = 5

        while opc != salir:

            print("=======>MENÚ<=======")
            print("\n(1)REGISTRAR NUEVO CLIENTE"
                  "\n(2)RENOVAR CLIENTE EXISTENTE"
                  "\n(3)ELIMINAR CLIENTE"
                  "\n(4)LISTAR CLIENTES"
                  "\n(5)SALIR\n")
            opc = int(input("===Ingrese un número: "))

            DML1 = DML()
            if opc == 1:

                DML1.registrarcliente()
                input("Cliente registrado con éxito."
                      "\nPRESIONE ENTER PARA VOLVER")

            if opc == 2:
                pass
            if opc == 3:
                pass
            if opc == 4:
                DML1.listarclientes()
            if opc == 5:
                break




